#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import render_template, Blueprint, g, abort, request, session
from app import app, bp, babel
from flask_babel import gettext

@babel.localeselector
def get_locale():
	print(request)
	lang = session.get('lang', 'not set')
	if lang:
		print(lang)
		return lang 
	else:
		print('Not founded')
		return 'ru'


@bp.route('/')
def index(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Solur - Завод алюминиевых профилей и радиаторов'
	session['lang'] = lang
	curr_url = "/"
	return render_template('index.html', title = title, lang = lang, curr_url = curr_url)

@bp.route('/catalog')
def catalog(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Solur - Завод алюминиевых профилей и радиаторов'
	session['lang'] = lang
	curr_url = "/catalog"
	return render_template('catalog.html', title = title, lang = lang, curr_url = curr_url)

@bp.route('/radiators')
def radiators(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Solur - Завод алюминиевых профилей и радиаторов'
	session['lang'] = lang
	curr_url = "/radiators"
	return render_template('radiators.html', title = title, lang = lang, curr_url = curr_url)

@bp.route('/radiators/bimetallic')
def bimetallic_radiators(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Solur - Завод алюминиевых профилей и радиаторов'
	session['lang'] = lang
	curr_url = "/radiators/bimetallic"
	return render_template('bimetalic.html', title = title, lang = lang, curr_url = curr_url)

@bp.route('/radiators/aluminium')
def aluminium_radiators(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Solur - Завод алюминиевых профилей и радиаторов'
	session['lang'] = lang
	curr_url = "/radiators/aluminium"
	return render_template('aluminium.html', title = title, lang = lang, curr_url = curr_url)


@bp.route('/profiles')
def profiles(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Solur - Завод алюминиевых профилей и радиаторов'
	session['lang'] = lang
	curr_url = "/profiles"
	return render_template('profiles.html', title = title, lang = lang, curr_url = curr_url)


@bp.route('/production')
def production(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Solur - Завод алюминиевых профилей и радиаторов'
	session['lang'] = lang
	curr_url = "/production"
	return render_template('production.html', title = title, lang = lang, curr_url = curr_url )

@bp.route('/contacts')
def contacts(lang):
	if lang and lang not in app.config['SUPPORTED_LANGUAGES'].keys():
		return abort(404)
	title = u'Контакты'
	session['lang'] = lang
	curr_url = "/contacts"
	return render_template('contacts.html', title = title, lang = lang, curr_url = curr_url )


@app.errorhandler(404) 
def not_found(e): 
	return render_template("404.html")




# pybabel compile -d app/translations compiling catalog app/translations/en/LC_MESSAGES/messages.po to app/translations/en/LC_MESSAGES/messages.mo
# pybabel compile -d app/translations compiling catalog app/translations/ru/LC_MESSAGES/messages.po to app/translations/ru/LC_MESSAGES/messages.mo


