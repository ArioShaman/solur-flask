from flask import Flask, Blueprint, session
from flask_babel import Babel
from app.config import  BaseConfig
from flask_session import Session


bp = Blueprint('main', __name__)

app = Flask(__name__)
app.secret_key = 'supersecretkey'
app.config.from_object(BaseConfig)

babel = Babel(app)

sess = Session()
sess.init_app(app)


from app import routes