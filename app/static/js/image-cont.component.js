var activeImages;



var activeTab = document.querySelector(".image-cont");
if (activeTab && activeTab !== null) {

    var activeTabId = activeTab.getAttribute("tab-id");
    
    var currentLenght = profilesLenght;
    var currentPage = profileActivePage;
    
    var activeTabName = 'profiles';
    
    var activeIdName;
    
    activeImages = profiles;
    
    
    var lazyLoadOptions = { 
        elements_selector: "img.lazy",
    };
    
    var aLazyLoad; 
    setTimeout(function() {
        aLazyLoad = new LazyLoad(lazyLoadOptions);
    }, 1);
    
    imageUpdate = function(){
        setTimeout(function(){
            aLazyLoad.update();
            
        },10);
    }
    
    buttons = function(){  
        $('.gal-arrow-left').click(function(){
            var curPage;
            switch (activeTabName){
                case 'profiles':
                    if(profileActivePage > 0 ){
                        profileActivePage--;
                        currentPage = profileActivePage;
                    }
                    break;
                case 'radiators':
                   if(radiatorsActivePage > 0){
                        radiatorsActivePage--;
                        currentPage = radiatorsActivePage;
                    }            
                    break;
                case 'splavs':
                   if(splavsActivePage > 0){
                        splavsActivePage--;
                        currentPage = splavsActivePage;
                    }               
                    break;      
                     
            }
            updateDom();
        });
    
        $('.gal-arrow-right').click(function(){
            var curPage;
            switch (activeTabName){
                case 'profiles':
                    if(profileActivePage < Math.round(profilesLenght/5) - 1){
                        profileActivePage++;
                    }
                    currentPage = profileActivePage;
                    break;
                case 'radiators':
                   if(radiatorsActivePage < Math.round(radiatorsLenght/5) - 1){
                        radiatorsActivePage++;
                        currentPage = radiatorsActivePage;
                    }            
                    break;
                case 'splavs':
                   if(splavsActivePage < Math.round(splavsLenght/5) - 1){
                        splavsActivePage++;
                        currentPage = splavsActivePage;
                    }               
                    break;      
                default:
                    break;                  
            }
            updateDom();
        });
    
        $('.img-el').click(function(){
            const width = window.innerWidth;
            $('html, body').css({'overflow': 'hidden'});
            const gallery = $('#seventh');
            let topOffset;
            if (gallery.offset() && gallery.offset() !== null) {
                topOffset = gallery.offset()['top'];
                    $('html, body').animate({scrollTop: topOffset + 50}, 10);        
                $('.modal-gallery').addClass('opened');
                activeIdName = $(this).attr('data-id'); 
                updateGalleryDom(activeIdName);
            } else {
                const radiators = $('#radiators');
                topOffset = radiators.offset()['top'];
                console.log('not main page', topOffset);

                $('html, body').animate({scrollTop: topOffset + 50}, 10);  
                $('.modal-gallery').addClass('opened');
                activeIdName = $(this).attr('data-id'); 
                updateGalleryDom(activeIdName);
            }
        });
    }
    
    disactivateTab = function(els){
        $('.tab-text.active').removeClass('active');
        $(els).addClass('active');
    
    }    
    
    $('.proff-butt').click(function(){
        disactivateTab(this);
        document.querySelector(".image-cont").setAttribute("tab-id", 1);
        activeImages = profiles;
        currentLenght = profilesLenght;
        currentPage = profileActivePage;
        activeTabName = 'profiles';
        updateDom();
    });
    
    $('.radio-butt').click(function(){
        disactivateTab(this);
        document.querySelector(".image-cont").setAttribute("tab-id", 2);
        activeImages = radiators;  
        currentLenght = radiatorsLenght;
        currentPage = radiatorsActivePage;
        activeTabName = 'radiators';
        updateDom();
    });   
    $('.splav-butt').click(function(){
        disactivateTab(this);
        document.querySelector(".image-cont").setAttribute("tab-id", 3);
        activeImages = splavs;
        currentLenght = splavsLenght;
        currentPage = splavsActivePage;
        activeTabName = 'splavs';
        updateDom();    
    });  
    
    updateDom = function(){
        var width = window.innerWidth;
        var innerHTML;
        if( width > 1024){
            innerHTML = '\
                   <div class="images-cont">\
                        <div class="line top-line">'
                            + (activeImages[currentPage][0] ? '<div data-id="'+activeImages[currentPage][0]['id']+'" class="line-el left-el img-el">\
                                        <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][0]['img']+'">\
                                </div>':'<div class="line-el left-el img-el"></div>') +
    
                            '\
                            <div class="line-el right-el">'+
                                (activeImages[currentPage][1] ? 
                                    '<div data-id="'+activeImages[currentPage][1]['id']+'" class="in-line line-el img-el">\
                                            <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][1]['img']+'">\
                                    </div>':'<div class="line-el left-el img-el"></div>'
                                ) +
                                (activeImages[currentPage][2] ?
                                    '<div data-id="' + activeImages[currentPage][2]['id'] +'" class="in-line line-el img-el">\
                                        <img src="static\\images\\blur.jpg" class="lazy" data-src="' + activeImages[currentPage][2]['img']+'">\
                                    </div>\
                                    ':'<div class="line-el left-el img-el"></div>'
                                ) +
                        
                        '</div>\
                        </div>\
                        \
                        <div class="line bottom-line">'+ 
                            (activeImages[currentPage][3] ? 
                                '<div data-id="'+activeImages[currentPage][3]['id']+'" class="line-el img-el">\
                                    <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][3]['img']+'">\
                                </div>':'<div class="line-el left-el img-el"></div>'
                            ) +
                            (activeImages[currentPage][4] ? 
                                '<div class="line-el img-el" data-id="'+activeImages[currentPage][4]['id']+'">\
                                    <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][4]['img']+'">\
                                </div>':'<div class="line-el left-el img-el"></div>'
                            )
                            +
                            '<div class="line-el gallery-remote">\
                                <div class="arrows-cont">\
                                    <div class="arrow arrow-left gal-arrow-left"></div>\
                                    <div class="text">\
                                        <span>'+ (currentPage + 1) + '</span> / <span>'+ (Math.round(currentLenght/5))+'</span>\
                                    </div>\
                                    <div class="arrow arrow-right gal-arrow-right"></div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>';            
        }else{
            innerHTML = '\
                <div class="images-cont">\
                    <div class="main-img">'+ 
                        (activeImages[currentPage][0] ? '<div data-id="'+activeImages[currentPage][0]['id']+'" class="line-el img-el">\
                                        <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][0]['img']+'">\
                                </div>':'<div class="line-el img-el"></div>')
                    
                    +'</div>\
                    \
                    <div class="line">\
                        <div class="line-el">' + 
                            (activeImages[currentPage][1] ? 
                                '<div data-id="'+activeImages[currentPage][1]['id']+'" class="line-el img-el">\
                                    <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][1]['img']+'">\
                                </div>':'<div class="line-el left-el img-el"></div>'
                            )                    
                        + '</div>\
                        <div class="line-el">' + 
                            (activeImages[currentPage][2] ? 
                                '<div data-id="'+activeImages[currentPage][2]['id']+'" class="line-el img-el">\
                                    <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][2]['img']+'">\
                                </div>':'<div class="line-el left-el img-el"></div>'
                            )
                        + '</div>\
                    </div>\
                    <div class="line">\
                        <div class="line-el">'+
                            (activeImages[currentPage][3] ? 
                                '<div data-id="'+activeImages[currentPage][3]['id']+'" class="line-el img-el">\
                                    <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][3]['img']+'">\
                                </div>':'<div class="line-el left-el img-el"></div>'
                            )                     
                        +'</div>\
                        <div class="line-el">'+ 
                            (activeImages[currentPage][4] ? 
                                '<div data-id="'+activeImages[currentPage][4]['id']+'" class="line-el img-el">\
                                    <img src="static\\images\\blur.jpg" class="lazy" data-src="'+activeImages[currentPage][4]['img']+'">\
                                </div>':'<div class="line-el left-el img-el"></div>'
                            ) 
                        +'</div>\
                    </div>\
                    \
                    <div class="line">\
                        <div class="line-el gallery-remote">\
                                <div class="arrows-cont">\
                                    <div class="arrow arrow-left gal-arrow-left"></div>\
                                    <div class="text">\
                                        <span>'+ (currentPage + 1) + '</span> / <span>'+ (Math.round(currentLenght/5))+'</span>\
                                    </div>\
                                    <div class="arrow arrow-right gal-arrow-right"></div>\
                                </div>\
                        </div>\
                    </div>\
                    \
                </div>\
            ';
        }
        $('.image-cont').html(innerHTML);
        imageUpdate();
    
    
        buttons();      
    }
    
    updateDom();

}    
