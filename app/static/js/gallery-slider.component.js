var profiles = []; var radiators = []; var splavs = [];
var profilesLenght = 34;
var profileActivePage = 0;

var radiatorsLenght = 6;
var radiatorsActivePage = 0;

var splavsLenght = 18;
var splavsActivePage = 0;

var activeListImages = [];
var counter = 1;

var galLazyLoadOptions = { 
    elements_selector: "img.gal-lazy",
};
var aGalLazyLoad; 
setTimeout(function() {
    aGalLazyLoad = new LazyLoad(galLazyLoadOptions);
}, 1);

galImageUpdate = function(){
    setTimeout(function(){
        aGalLazyLoad.update();
    },10);
}

counter = 1;
for(var j = 0; j < profilesLenght/5; j++){
    profiles.push([]);
    for(var i = 0; i < 5; i++){
        if( counter < profilesLenght){
            profiles[j].push(
                {
                    id: 'profile-'+counter,
                    img: 'static\\images\\gallery\\1\\'+counter+'.jpg'
                }
            );
        }
        counter++;
    }
}

counter = 1;
for(var j = 0; j < radiatorsLenght/5; j++){
    radiators.push([]);
    for(var i = 0; i < 5; i++){
        if( counter < radiatorsLenght){
            radiators[j].push(
                {
                    id: 'radiator-'+counter,
                    img: 'static\\images\\gallery\\2\\'+counter+'.jpeg'
                }
            );
        }
        counter++;
    }
}

counter = 1;
for(var j = 0; j < splavsLenght/5; j++){
    splavs.push([]);
    for(var i = 0; i < 5; i++){
        if( counter < splavsLenght){
            splavs[j].push(
                    {
                        id: 'splav-'+counter,
                        img: 'static\\images\\gallery\\3\\'+counter+'.jpg'
                    }
                );
        }
        counter++;
    }
}

var galleryHTML;


getImgById = function(list, imgId){
    console.log();
    for(var i = 0; i < list.length; i++){
        for(var j = 0; j < list[i].length; j++){
            if (list[i][j]['id'] == imgId){
                return list[i][j]['img']; 
            }
        }

    }
}

updateGalleryDom  = function(imgId){
    var defis = imgId.indexOf("-");
    var nameOfArray = imgId.substring(0, defis);
    var id = imgId.substr(defis+1, 2);
    id = parseInt(id);
    var listLength;
    switch(nameOfArray){
        case 'profile':
            activeListImages = profiles;
            listLength = profilesLenght;
            break;
        case 'radiator':
            activeListImages = radiators;
            listLength = radiatorsLenght;
            break;
        case 'splav':
            activeListImages = splavs;
            listLength = splavsLenght;
            break;            
    } 
    listLength--;
    var img = getImgById(activeListImages, imgId);
    galleryHTML = '\
        <div class="gal-arrows-cont">\
            <div class="arrows">\
                <div class="arrow arrow-left gal-arrow-left"></div>\
                <div class="arrow arrow-right gal-arrow-right"></div>\
            </div>\
        </div>\
        \
        <div class="img-container">\
            <div class="img-box">' +
                '<img src="static\\images\\blur.jpg" class="gal-lazy" data-src="'+img+'"> ' +
            '</div>\
        </div>\
        <div class="remote">\
            <span>' +id + '</span> / <span>'+listLength+'</span>\
        </div>';

    $('.gallery-slider-cont').html(galleryHTML);    
    galImageUpdate();

    $('.gal-arrow-right').click(function(){
        if(id < listLength){
            id++;
            imgId = nameOfArray + '-' + id;
            updateGalleryDom(imgId);
        }
    });

    $('.gal-arrow-left').click(function(){
        if(id > 1){
            id--;
            imgId = nameOfArray + '-' + id;
            updateGalleryDom(imgId);
        }
    });
}