$(document).ready(function() {
    $('.photos').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: false,
        touchMove: false,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        lazyLoad: 'ondemand',
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.photos',
        centerMode: true,
        focusOnSelect: true
    });
});