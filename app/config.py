class BaseConfig(object):
	SUPPORTED_LANGUAGES = {
		'ru': 'Russian',
		'en': 'English'
	}
	BABEL_DEFAULT_LOCALE = 'ru'
	SESSION_TYPE = 'filesystem'