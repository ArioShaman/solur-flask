var gulp = require('gulp'),
sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer'),
minifycss = require('gulp-minify-css'),
rename = require('gulp-rename'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
order = require("gulp-order"),
sassmixins = require('gulp-sass-to-postcss-mixins');

gulp.task('styles', function() {
  return gulp.src('./app/static/sass/*.scss')
  .pipe(sass({ style: 'expanded' }))
  .pipe(sassmixins())
  .pipe(minifycss())
  .pipe(autoprefixer())
  .pipe(concat('main.min.css'))
  .pipe(gulp.dest('./app/static/css'));
});


gulp.task('watch', function() {
  gulp.watch('./app/static/sass/*.scss', gulp.series('styles'));
  //gulp.watch('static/js/*.js',['scripts']);
});

