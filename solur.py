from app import app, bp, cli
from flask import Flask, Blueprint

app.register_blueprint(bp, url_defaults={'lang': 'ru'})
app.register_blueprint(bp, url_prefix='/<lang>')

application = app

if __name__ == '__main__':
    application.run(host = '0.0.0.0', port = '985')